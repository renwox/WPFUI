﻿using System.Windows;
using TemplateClassLibrary.ClassComponent;
using WpfUI.WpfComponents;

namespace WpfUI
{
    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Maximized;//还原窗口（非最小化和最大化）
            //this.WindowStyle = System.Windows.WindowStyle.None;仅工作区可见，不显示标题栏和边框
            //this.ResizeMode = System.Windows.ResizeMode.NoResize;不显示最大化和最小化按钮
            this.Topmost = false;//窗口在最前
            this.Left = 0.0;
            this.Top = 0.0;
            this.Width = System.Windows.SystemParameters.PrimaryScreenWidth;
            this.Height = System.Windows.SystemParameters.PrimaryScreenHeight;
            //PopUp p = new PopUp();
            //p.Show();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            BtnExample btnType = new BtnExample();
            this.content.Children.Add(btnType);
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            IconList icons = new IconList();
            this.content.Children.Add(icons);
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            HaS haS = new HaS();
            this.content.Children.Add(haS);
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            table table = new table();
            this.content.Children.Add(table);
        }

        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            Input input = new Input();
            this.content.Children.Add(input);
        }

        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            RadioBtnExample radioBtnExample = new RadioBtnExample();
            this.content.Children.Add(radioBtnExample);
        }

        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            CheckBoxBtnExample checkBoxBtnExample = new CheckBoxBtnExample();
            this.content.Children.Add(checkBoxBtnExample);
        }

        private void Button_Click_7(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            InputExample inputExample = new InputExample();
            this.content.Children.Add(inputExample);
        }

        private void Button_Click_8(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            DateExample dateExample = new DateExample();
            this.content.Children.Add(dateExample);
        }

        private void Button_Click_9(object sender, RoutedEventArgs e)
        {
            this.content.Children.Clear();
            LoadingExample dateExample = new LoadingExample();
            this.content.Children.Add(dateExample);
            
        }
    }
}
